import cloudscraper
import json
import pandas as pd

pd.options.mode.copy_on_write = True


def process_raw_data(raw_data):
    try:

        df = raw_data[["Id", "Name", "cover", "FullUrl", "MinTicketPrice", "MaxTicketPrice", "Currency", "OrganizerName", "IsDataSearchFromGoogle"]]
        df.loc[:, "address"] = raw_data.loc[:, "Venue"].map(dict).str["Address"]
        df.loc[:, "city"] = raw_data["Venue"].map(dict).str["City"]
        df.loc[:, "place_name"] = raw_data["Venue"].map(dict).str["Name"]
        df.loc[:, "cat_id"] = raw_data["Categories"].apply(lambda x: ",".join([str(_["Id"]) for _ in x]))
        df.loc[:, "cat_name"] = raw_data["Categories"].apply(lambda x: ",".join([str(_["Name"]) for _ in x]))
        df.loc[:, "start_time"] = raw_data["Showings"].apply(lambda x: "".join([str(_["StartTime"]) for _ in x]))
        df.loc[:, "end_time"] = raw_data["Showings"].apply(lambda x: "".join([str(_["EndTime"]) for _ in x]))

        df.rename(
            columns={"Id": "id", "Name": "name", "FullUrl": "full_url", "MinTicketPrice": "min_ticket_price", "MaxTicketPrice": "max_ticket_price", "Currency": "currency",
                     "OrganizerName": "organizer_name", "IsDataSearchFromGoogle": "is_data_search_from_google"},
            inplace=True,
        )

        return df
    except Exception as e:
        print(e)


def main():
    data = {
        "CityName": "ha-noi",
        "CityId": 1,
        "DateFilter": 1,
        "Price": 3,
        "SiteId": 1,
        "CultureName": "",
        "Offset": 0,
        "Limit": 100,
        "TotalCount": 100,
        "UserIp": "",
        "UserAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36",
        "categories": "8,9,10,11,12,3,7,13"
    }

    url = "https://ticketbox.vn/EventList/EventList/LoadEventListData?" \
          "Opm=&Q=&CityName={CityName}" \
          "&OtherLocation=false" \
          "&CityId={CityId}" \
          "&From=&To=&DateFilter=1&Price=3&SiteId=1&CultureName=&Offset=0" \
          "&Limit={Limit}" \
          "&TotalCount={TotalCount}" \
          "&UserIp=172.30.3.159" \
          "&UserAgent={UserAgent}" \
          "&categories=8,9,10,11,12,3,7,13".format(CityName=data["CityName"], CityId=data["CityId"], Limit=data["Limit"], TotalCount=data["TotalCount"],
                                                   UserAgent=data["UserAgent"], categories=data["categories"])
    scraper = cloudscraper.create_scraper()

    headers = {}

    try:
        resp = json.loads(scraper.get(url, headers=headers).text)
        resp_df = pd.DataFrame(resp)
        data = process_raw_data(resp_df)
        return data
    except Exception as e:
        print(e)
        return


if __name__ == '__main__':
    main()
